<?php

    namespace controllers;
    use components\Controller;

    class SiteController extends Controller
    {
        public function actionMain()
        {
            $products = $this->model->getProducts();
            $featureNames = $this->model->getFeatureNames2();

            require_once(ROOT.'/views/site.php');
            return true;
        }

        public function actionAddShow()
        {
            $categories = $this->model->getCategories();
            require_once(ROOT.'/views/product_add.php');
            return true;
        }

        public function actionOnChange($type)
        {
            $measureKey = $this->model->getKey($type);
            $featureNames = $this->model->getFeatureNames2();
            require_once(ROOT.'/views/layouts/inputs/'.$type.'_input.php');
        }
    }

?>