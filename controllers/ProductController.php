<?php

    namespace controllers;
    use components\Controller;

    class ProductController extends Controller
    {

        public function actionAddProduct()
        {
            if(isset($_POST['save']))
            {
                $name = (string)$_POST['name'];
                $price = (float)$_POST['price'];
                $type = (string)$_POST['type'];
                $feature = (string)implode('x', $_POST['feature']);
                $this->model->addNewProduct($name, $price, $type, $feature);
            }
            header("Location: / ");
        }

        public function actionDeleteChecked()
        {
            if(isset($_POST['delete_checked']))
            {
                $idArray = explode(" ", $_POST['sku_to_delete']);
                $this->model->deleteChecked($idArray);
            }
            header("Location: / ");
        }

        public function actionNameValidation()
        {
            $name = $_POST['nameVal'];
            $check = array("?", "/", ">", "<", "]", "[", ")", "(", "}", "{");
            $nameSplited = str_split($name);
            $isIn = false;
            if(strlen($name) > 1)
            {
                foreach($nameSplited as $letter)
                {
                    foreach($check as $item)
                    {
                        if($letter === $item)
                        {
                            $isIn = true;
                        }
                    }
                }
                if($isIn)
                {
                    echo '<p class="error"> AVOID USING ?, /, >, <, ], [, ), (, }, {</p>';
                }
                else
                {
                    echo '<p class="ok"> OK</p>';
                }
            }
            else
            {
                echo '<p class="error"> NAME MUST BE MORE THAN 1 CHARACTERS</p>';
            }
            return true;
        }

        public function actionPriceValidation()
        {
            $price = $_POST['priceVal'];
            if(preg_match('/^[0-9]+(\.[0-9]{1,2})?$/', $price))
            {
                echo '<p class="ok"> OK</p>';
            }
            else
            {
                echo '<p class="error"> WRONG PRICE</p>';
            }
            return true;
        }

        public function actionFeatureValidation()
        {
            $feature = $_POST['featureVal'];
            $type = $_POST['typeVal'];
            $error = false;

            foreach($feature as $value)
            {
                if(!preg_match('/^[0-9]+(\.[0-9]{1,2})?$/', $value))
                {
                    $error = true;
                }
            }

            if($error === false)
            {
                echo '<p class="ok"> OK</p>';
            }
            else
            {
                $errors = $this->model->getFeatureErrors();
                $errorLabel = $this->model->getError($errors, $type);
                echo $errorLabel;
            }
            return true;
        }
    }
?>