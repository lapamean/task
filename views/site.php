<?php
    require_once(ROOT.'/views/layouts/header.php');
?>
<div class="container">
    <div class="side_menu">
        <?php require_once(ROOT.'/views/layouts/side_menu.php');?>
    </div>
    <div class="products">
        <div class="content">
            <h2>Products</h2>
            <div class="page">
                <?php foreach ($products as $productItem): ?>
                    <div class="product">
                        <div class="prod-man">
                            <form action="include.php" class = "checkbox" method="GET">
                                <input type="checkbox" id="product<?php echo htmlspecialchars($productItem['id']);?>" value="<?php echo htmlspecialchars($productItem['id']); ?>">
                                <?php            
                                    echo '<p> #product '.$productItem['id'].'</p>';
                                ?>
                            </form>
                        </div>
                        <div class="description">
                            <p>Name: <?php echo $productItem['name'];?></p>
                            <?php
                                echo '<p>'.ucfirst($featureNames[$productItem['measure']]).': '.$productItem['value'].' '.$productItem['measure'];
                            ?>
                            <p>Price: <?php echo $productItem['price']?> $</p>
                        </div>
                    </div>
                <?php endforeach?>
            </div>
        </div>
    </div>
</div>
<?php  require_once(ROOT.'/views/layouts/footer.php'); ?>
