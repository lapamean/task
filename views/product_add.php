<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="../template/css/style.css" type="text/css"/>
        <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script> -->
        <script src="../template/js/jquery.js"></script>
        <title>Test</title>
    </head>
    <body>
        <div class="overlay">
            <div class="modal_window">
                <a href="/"><div class="close">
                    <span></span>
                    <span></span>
                </div></a>
                <div class="title">ADD NEW PRODUCT</div>
                <hr>
                <div class="wrapper">
                    <form name="upload" action="/product/add" method="POST" class="form">
                        <a>
                            <label for="">NAME</label> 
                            <input type="text" name="name" class="name" Required> 
                            <span class="error_form" id="name_error"></span>
                        </a>
                        <a>
                            <label for="">PRICE</label> 
                            <input type="text" name="price" class="price" Required>, $ 
                            <span class="error_form" id="price_error"></span>
                        </a>
                        <a>
                            <label for="">TYPE</label>
                            <select name="type" class="guns_type" Required>
                                <option value=""></option>
                                <?php foreach ($categories as $category): ?>
                                    <?php $category['name'] = substr_replace($category['name'],"",-1);?>
                                    <option value="<?php echo $category['name'];?>"><?php echo $category['name'];?></option>
                                <?php endforeach; ?>
                            </select>
                        </a>
                        <a class="feature">
                        </a>
                        <input type="submit" name="save" value="APPLY AND SAVE" class="submit">
                    </form>
                </div>
            </div>
        </div>
    </body>
    <script src="../template/js/script.js"></script>
    <script src="../template/js/add_product_valid.js"></script>
</html>        