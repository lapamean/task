<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="../template/css/style.css" type="text/css"/>
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script> -->
    <script src="../template/js/jquery.js"></script>
    <title>Test</title>
</head>
<body>
    <div class="header">
        <div class="header2">
            <div class="nav">
                <a href="#">About Us</a>
                <a href="#">Payment</a>
                <a href="#">Delivery</a>
                <a href="#">Return & Warranty</a>
                <a href="#">Information about the order</a>
            </div>
        </div>
    </div>