$(document).ready(function(){
    $('.delete').click(function(){
        var deleteModalWindow = $('.delete_overlay');
        deleteModalWindow.fadeIn();
    });

    $('.close_del').click(function(){
        var deleteModalWindow = $('.delete_overlay');
        deleteModalWindow.fadeOut();
    });

    $('.cdelete').submit(function(){
        var array = $.map($('input[type="checkbox"]:checked'), function(c){return c.value; });
        var index = array.length;
        if(index == 0)
        {
            alert("No products to delete, please check");
            return false;
        }
        var result = "";
        for(var i = 0; i < index; i++){
            result = result + array[i] + " ";
        }
        console.log(result);
        $("#sku_to_delete").val(result);
        return true;
    });
});