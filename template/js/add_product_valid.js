$(document).ready(function(){

    $('.form').submit(function(){
        
        checkName();
        checkPrice();
        checkFeature();

        var name_error = $('#name_error').find('.ok').length;
        var price_error = $('#price_error').find('.ok').length;
        var feature_error = $('#feature_error').find('.ok').length;

        if(name_error > 0 && price_error > 0 && feature_error > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    });

    function getValue()
    {
        var values = [];
        $('input[type="text"].feat-ures').each(function(){
            if($(this).val() != '')
            {
                values.push($(this).val());
            }
        });
        return values;
    }

    $('select.guns_type').change(function(){
        var selectedType = $(this).children("option:selected").val();
        console.log(selectedType);
        if(selectedType != "")
        {
            $.ajax({
                url: '/product/change/'+selectedType,
                method: 'POST',
                data:{},
                success: function(response)
                {
                    $(".feature").html(response);
                }
            });
        }
        else
        {
            $(".feature").html("");
        }
    });

    function checkName()
    {
        var name = $('.name').val();
        if(name != '')
        {
            $.ajax({
                url: '/product/name_valid',
                method: 'POST',
                data:{nameVal:name},
                success: function(response)
                {
                    $("#name_error").html(response);
                }
            });
        }
        else
        {
            $("#name_error").html("");
        }
    }

    function checkPrice()
    {
        var price = $('.price').val();
        if(price != '')
        {
            $.ajax({
                url: '/product/price_valid',
                method: 'POST',
                data:{priceVal:price},
                success: function(response)
                {
                    $("#price_error").html(response);
                }
            });
        }
        else
        {
            $("#price_error").html("");
        }
    }

    function checkFeature()
    {
        var type = $('.feat-ures').data('id');
        var feature = getValue();
        if(feature.length == $('input[type="text"].feat-ures').length)
        {
            $.ajax({
                url: '/product/feature_valid',
                method: 'POST',
                data:{featureVal:feature, typeVal:type},
                success: function(response)
                {
                    $("#feature_error").html(response);
                }
            });
        }
        else
        {
            $("#feature_error").html("");
        }
    }

    $('.name').focusout(function(){
        checkName();
    });

    $('.price').focusout(function(){
        checkPrice();
    });

    $('.feature').focusout(function(){
        checkFeature();
    });

});