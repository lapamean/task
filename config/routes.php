<?php

    return array(
        'product/change/([\s\S]+)' => 'site/onChange/$1',
        'product/name_valid' => 'product/nameValidation',
        'product/price_valid' => 'product/priceValidation',
        'product/feature_valid' => 'product/featureValidation',
        'product/cdelete' => 'product/deleteChecked',
        'product/add' => 'product/addProduct',            
    
        'add/overlay' => 'site/addShow',               
        '' => 'site/main'
    );

?>