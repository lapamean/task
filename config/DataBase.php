<?php

    namespace config;
    use mysqli;

    class DataBase
    {
        public static function getConnection()
        {
            $host = "localhost";
            $user = "root";
            $password = "";
            $db = "testdb";

            return new mysqli($host, $user, $password, $db);
        }
    }

?>