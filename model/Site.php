<?php

    namespace model;
    use components\Model;
    use config\DataBase;

    class Site extends Model
    {
        public function getProducts()
        {
            $products = array();
            $sql = DataBase::getConnection();

            $result = $sql -> prepare(" SELECT product.id, product.name, product.price, product.value, category.measure 
                                        FROM product 
                                        LEFT JOIN category 
                                        ON product.category_id = category.id 
                                        ORDER BY product.id");
            $result -> execute();
            $result = $result -> get_result();

            $i = 0;
            while($product = mysqli_fetch_assoc($result))
            {
                $products[$i]['id'] = $product['id'];
                $products[$i]['name'] = $product['name'];
                $products[$i]['measure'] = $product['measure'];
                $products[$i]['price'] = $product['price'];
                $products[$i]['value'] = $product['value'];
                $i++;
            }
            $sql -> close();
            return $products;
        }

        public function getKey($type)
        {
            $sql = DataBase::getConnection();
            $type = $type.'s';

            $result = $sql -> prepare(" SELECT category.measure 
                                        FROM category 
                                        WHERE category.name = ?");
            $result -> bind_param('s', $type);
            $result -> execute();
            $result = $result -> get_result();

            $categoryMeasure = mysqli_fetch_assoc($result);
            $measure = $categoryMeasure['measure'];
            $sql -> close();
            return $measure;
        }
    }
?>