<?php

    namespace model;
    use components\Model;
    use config\DataBase;

    class Product extends Model
    {

        public function addNewProduct($name, $price, $type, $feature)
        {
            $sql = DataBase::GetConnection();
            $type = $type.'s';

            $result = $sql -> prepare(" SELECT category.id 
                                        FROM category 
                                        WHERE category.name = ?");
            $result -> bind_param('s', $type); 
            $result -> execute();
            $result = $result -> get_result();

            $category_id =  mysqli_fetch_assoc($result);
            
            $result = $sql -> prepare("INSERT INTO testdb . product (product.name, product.category_id, product.price, product.value) 
                                       VALUES (?, ?, ?, ?)");
            $result -> bind_param('sids', $name, $category_id['id'], $price, $feature); 
            $result -> execute();

            $sql -> close();
            return true;
        }

        public function deleteChecked(array $idArray)
        {
            $sql = DataBase::GetConnection();

            foreach($idArray as $id)
            {
                $result = $sql -> prepare("DELETE FROM product 
                                           WHERE product.id = ?");
                $result -> bind_param('i', $id);   
                $result -> execute(); 
            }
            $sql -> close();
            return true;
        }

        public function getFeatureErrors()
        {
            return array(
                'XYZ' => '<p class="error" id="furniture-error"> WRONG DIMENSION, ex: 2x6x7, 10x4x20</p>',
                'kg' => '<p class="error" id="book-error"> WRONG WEIGHT, ex: 100, 50, 75.25</p>',
                'mb' => '<p class="error" id="disc-error"> WRONG CAPACITY, ex: 1000, 500, 75.25</p>'
            );
        }

        public function getError($errors, $type)
        {
            $sql = DataBase::GetConnection();
            $type = $type.'s';

            $result = $sql -> prepare(" SELECT category.measure 
                                        FROM category 
                                        WHERE category.name = ?");
            $result -> bind_param('s', $type);   
            $result -> execute();
            $result = $result -> get_result();

            $error = mysqli_fetch_assoc($result);

            $sql -> close();
            return $errors[$error['measure']];
        }
    }
?>