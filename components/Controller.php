<?php

    namespace components;

    class Controller
    {
        protected $model;

        public function __construct($model)
        {
            $modelName = ucfirst($model);
            require_once(ROOT.'/model/'.$modelName.'.php');
            $path = '\model\\'.$modelName;
            $this->model = new $path;
        }
    }

?>