<?php

    namespace components;

    class Router
    {
        private $routes;
        private $params;
        private $controller;
        private $action;

        public function __construct()
        {
            require_once(ROOT.'/components/Controller.php');
            require_once(ROOT.'/components/Model.php');
            $this->routes = require_once(ROOT.'/config/routes.php');
        }

        public function match()
        {
            $uri = trim($_SERVER['REQUEST_URI'], '/');

            foreach ($this->routes as $uriPattern => $path)  
            {
                if(preg_match('#^'.$uriPattern.'$#', $uri))
                {
                    $finallRoute = preg_replace('#^'.$uriPattern.'$#', $path, $uri);
                    $segments = explode('/', $finallRoute); 

                    $this->controller = ucfirst(array_shift($segments).'Controller');
                    $this->action = 'action'.ucfirst(array_shift($segments));         
                    $this->params = $segments;
                    return true;
                }
            }
            return false;
        }

        public function run()
        {
            if($this->match())
            {
                $model = str_replace('Controller', "", $this->controller);

                $controllerFile = ROOT.'/controllers/'. $this->controller . '.php';
                if(file_exists($controllerFile))
                {
                    require_once($controllerFile);
                    $this->controller = '\controllers\\'.$this->controller;
                    $object = new $this->controller($model);
                    call_user_func_array(array($object, $this->action), $this->params);

                    $_SESSION['current_url'] = $_SERVER['REQUEST_URI'];
                }
                else
                {
                    header('Location: '.$_SESSION['current_url']);
                }
                return true;
            }
            else
            {
                header('Location: '.$_SESSION['current_url']);
                return true;
            }
        }
    }
?>