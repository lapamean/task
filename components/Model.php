<?php

    namespace components;
    use config\DataBase;

    class Model
    {

        public function __construct()
        {
            require_once(ROOT . '/config/DataBase.php');
        }

        public function getFeatureNames2()
        {
            return array(
                'XYZ' => 'dimension',
                'kg' => 'weight',
                'mb' => 'capacity'
            );
        }

        public function getCategories()
        {
            $categories = array();
            $status = 1;
            $sql = DataBase::getConnection();

            $result = $sql -> prepare(" SELECT category.name 
                                        FROM category
                                        WHERE category.status = ?");
            $result -> bind_param('i', $status);                           
            $result -> execute();
            $result = $result -> get_result();

            $i = 0;
            while($product = mysqli_fetch_assoc($result))
            {
                $categories[$i]['name'] = $product['name'];
                $i++;
            }
            $sql -> close();
            return $categories;
        }
    }

?>